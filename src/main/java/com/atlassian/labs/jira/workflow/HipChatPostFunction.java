package com.atlassian.labs.jira.workflow;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.labs.hipchat.HipChatProxyClient;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.query.Query;
import com.atlassian.sal.api.ApplicationProperties;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.StringTokenizer;

public class HipChatPostFunction extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(HipChatPostFunction.class);
    public static final String ROOMS_TO_NOTIFY = "roomsToNotifyStr";
    private final HipChatProxyClient hipChatProxyClient;
    private final SearchService searchService;
    private final ApplicationProperties applicationProperties;

    public HipChatPostFunction(ConfigurationManager configurationManager, ApplicationProperties applicationProperties,
                               SearchService searchService, HipChatProxyClient hipChatProxyClient)
    {
        this.applicationProperties = applicationProperties;
        this.hipChatProxyClient = hipChatProxyClient;
        this.searchService = searchService;
    }

    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        boolean shouldNotify = true;

        Issue issue = getIssue(transientVars);

        WorkflowDescriptor descriptor = (WorkflowDescriptor) transientVars.get("descriptor");
        Integer actionId = (Integer) transientVars.get("actionId");
        ActionDescriptor action = descriptor.getAction(actionId);
        Issue originalIssue = (Issue) transientVars.get("originalissueobject");
        String firstStepName = "";
        if (originalIssue != null) {
            Status status = originalIssue.getStatusObject();
            firstStepName = "<b>" + StringEscapeUtils.escapeHtml(status.getName()) + "</b>&rarr;";
        }

        String actionName = action.getName();
        StepDescriptor endStep = (StepDescriptor) descriptor.getStep(action.getUnconditionalResult().getStep());

        String roomsToNotify = (String) args.get(ROOMS_TO_NOTIFY);

        if (null == roomsToNotify) {
            shouldNotify = false; // if no rooms, don't notify
        }

        String jql = (String) args.get(HipChatPostFunctionFactory.JQL_FIELD);
        if (shouldNotify && jql != null && !jql.trim().equals("")) {
            shouldNotify = matchesJql(jql, issue, getCaller(transientVars, args)); // if doesn't match JQL, don't notify
        }

        if (shouldNotify) {
            User updatedByUser = getCaller(transientVars, args);
            String currentAssignee = "";
            if (issue.getAssignee() != null) {
                currentAssignee = " Current assignee is <a href=\"" + applicationProperties.getBaseUrl() +
                        "/secure/ViewProfile.jspa?name=" + StringEscapeUtils.escapeHtml(issue.getAssignee().getName()) +
                        "\">" + issue.getAssignee().getDisplayName() + "</a>.";
            }
            String msg ="<img src=\"" +
                    applicationProperties.getBaseUrl() + issue.getIssueTypeObject().getIconUrl() + 
                    "\" width=16 height=16 />&nbsp;" +
                    "<a href=\"" + applicationProperties.getBaseUrl() + "/browse/" +
                    issue.getKey() + "\"><b>" + issue.getKey() + ":</b> " + issue.getSummary() +
                    "</a> " + firstStepName + "<em>" + StringEscapeUtils.escapeHtml(actionName) +
                    "</em>&rarr;<b>" + StringEscapeUtils.escapeHtml(endStep.getName()) + "</b>" +
                    " by <a href=\"" + applicationProperties.getBaseUrl() +
                    "/secure/ViewProfile.jspa?name=" + StringEscapeUtils.escapeHtml(updatedByUser.getName()) + "\">" +
                    StringEscapeUtils.escapeHtml(updatedByUser.getDisplayName()) + "</a>." + currentAssignee;

            StringTokenizer rooms = new StringTokenizer(roomsToNotify, ",");

            while (rooms.hasMoreTokens()) {
                hipChatProxyClient.notifyRoom(rooms.nextToken(), msg);
            }
        }
    }

    private boolean matchesJql(String jql, Issue issue, User caller)
    {
        SearchService.ParseResult parseResult = searchService.parseQuery(caller, jql);
        if (parseResult.isValid()) {
            Query query = JqlQueryBuilder.newBuilder(parseResult.getQuery())
                    .where()
                    .and()
                    .issue()
                    .eq(issue.getKey())
                    .buildQuery();
            try {
                return searchService.searchCount(caller, query) > 0;
            } catch (SearchException e) {
                log.error("Error processing JQL: " + e.getMessage(), e);
                return false;
            }
        }

        return false;
    }
}