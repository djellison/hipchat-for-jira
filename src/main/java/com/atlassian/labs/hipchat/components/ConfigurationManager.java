package com.atlassian.labs.hipchat.components;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class ConfigurationManager {
    private static final String PLUGIN_STORAGE_KEY = "com.atlassian.labs.hipchat";
    private static final String HIPCHAT_AUTH_TOKEN_KEY = "hipchat-auth-token";

    private final PluginSettingsFactory pluginSettingsFactory;

    public ConfigurationManager(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public String getHipChatAuthToken() {
        return getValue(HIPCHAT_AUTH_TOKEN_KEY);
    }
    
    public String getHipChatRooms(String spaceKey){
        return getValue(spaceKey);
    }

    private String getValue(String storageKey) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        Object storedValue = settings.get(storageKey);
        return storedValue == null ? "" : storedValue.toString();
    }

    public void updateConfiguration(String authToken) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(HIPCHAT_AUTH_TOKEN_KEY, authToken);
    }

    public void setNotifyRooms(String spaceKey, String rooms) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(spaceKey, rooms);
    }


}